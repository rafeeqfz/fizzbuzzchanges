﻿namespace FizzBuzzWeb.Tests
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzzWeb.Controllers;
    using FizzBuzz.Services.Interfaces;
    using FizzBuzzWeb.Models;
    using Moq;
    using NUnit.Framework;
    
    [TestFixture]
    public class FizzBuzzControllerTests
    {
        private Mock<IFizzBuzzService> fizzBuzz;

        [SetUp]
        public void Setup()
        {
            this.fizzBuzz = new Mock<IFizzBuzzService>();
        }

        [Test]
        public void GetIndexTest()
        {
            FizzBuzzController controller = new FizzBuzzController(this.fizzBuzz.Object);
            var result = controller.Index(new FizzBuzzModel()) as ViewResult;
            var actualResult = result.ViewName;

            Assert.AreEqual("Index", actualResult);
        }

        [TestCase(5, (new string[] { "1", "2", "fizz", "4", "buzz" }))]
        public void PostIndexTest(int value, string[] expectedResult)
        {
            this.fizzBuzz.Setup(x => x.GetResult(It.IsAny<int>())).Returns(expectedResult);
            var controller = new FizzBuzzController(this.fizzBuzz.Object);

            var result = controller.Index(new FizzBuzzModel() { Input = value }) as ViewResult;
            var model = result.Model as FizzBuzzModel;

            var actualResult = new List<string>(model.FizzBuzzNumbers);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCase(5, (new string[] { "1", "2", "fizz", "4", "buzz" }))]
        [TestCase(25, (new string[] { "fizz", "22", "23", "fizz", "buzz" }))]
        public void PaginationTest(int value, string[] expectedResult)
        {
            this.fizzBuzz.Setup(x => x.GetResult(It.IsAny<int>())).Returns(expectedResult);
            FizzBuzzController controller = new FizzBuzzController(this.fizzBuzz.Object);

            var output = controller.Navigate(value, 1) as ViewResult;
            var model = output.Model as FizzBuzzModel;

            var actualResult = new List<string>(model.FizzBuzzNumbers);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCase(-1, (new string[] { "The Number Should be Between 1 to 1000" }))]
        [TestCase(1205, (new string[] { "The Number Should be Between 1 to 1000" }))]
        public void ModelStateValidationTest(int value, string[] expectedResult)
        {
            this.fizzBuzz.Setup(x => x.GetResult(It.IsAny<int>())).Returns(expectedResult);
            var controller = new FizzBuzzController(this.fizzBuzz.Object);

            var output = controller.Index(new FizzBuzzModel() { Input = value }) as ViewResult;
            var model = output.Model as FizzBuzzModel;
            var result = model.FizzBuzzNumbers;

            var actualResult = result;
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
