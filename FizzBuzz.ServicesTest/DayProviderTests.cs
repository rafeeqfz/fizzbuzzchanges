﻿namespace BusinessLogic
{
    using System;
    using FizzBuzz.Services.Implementations;
    using NUnit.Framework;

    [TestFixture]
    public class DayProviderTests
    {
        [TestCase(true, DayOfWeek.Wednesday)]
        [TestCase(false, DayOfWeek.Thursday)]
        [TestCase(false, DayOfWeek.Tuesday)]
        public void DayOfWeekProviderTest(bool day, DayOfWeek dayCheck)
        {
            var dateProvider = new DayProvider();
            var actualResult = dateProvider.IsValid(dayCheck);
            Assert.AreEqual(actualResult, day);
        }
    }
}