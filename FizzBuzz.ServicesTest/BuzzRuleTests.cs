﻿namespace BusinessLogic
{
    using System;
    using FizzBuzz.Services.Constants;
    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interfaces;
    using Moq;
    using NUnit.Framework;
   
    [TestFixture]
    public class BuzzRuleTests
    {
        private Mock<IDayProvider> dayProvider;

        [SetUp]
        public void Setup()
        {
            this.dayProvider = new Mock<IDayProvider>();
        }

        [TestCase(1, false)]
        [TestCase(5, true)]
        [TestCase(7, false)]
        public void IsValid_ShouldReturnTrueIfSatisfied(int value, bool expectedResult)
        {
            var result = new BuzzRule(this.dayProvider.Object);
            var actualResult = result.IsValid(value);
            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestCase(false, Constants.Buzz)]
        [TestCase(true, Constants.Wuzz)]
        public void GetValue_WuzzIfWednesday_WizzIfNotWednesday(bool day, string expectedResult)
        {
            var setUpValue = this.dayProvider.Setup(x => x.IsValid(DateTime.Now.DayOfWeek)).Returns(day);
            var result = new BuzzRule(this.dayProvider.Object);
            var actualResult = result.GetContent();
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
