﻿namespace BusinessLogic
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzz.Services.Constants;
    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interfaces;
    using Moq;
    using NUnit.Framework;

    public class FizzBuzzServiceTests
    {
        private Mock<IFizzBuzzRule> fizzBuzzRule;

        private Mock<IFizzBuzzRule> fizzRule;

        private Mock<IFizzBuzzRule> buzzRule;

        [SetUp]
        public void Setup()
        {
            this.fizzBuzzRule = new Mock<IFizzBuzzRule>();
            this.fizzRule = new Mock<IFizzBuzzRule>();
            this.buzzRule = new Mock<IFizzBuzzRule>();
        }

        [TearDown]
        public void TearDown()
        {
            this.fizzBuzzRule.ResetCalls();
            this.fizzRule.ResetCalls();
            this.buzzRule.ResetCalls();
        }

        [Test]
        public void FizzBuzzTestForZero()
        {
            this.fizzBuzzRule.Setup(x => x.IsValid(0)).Returns(false);
            this.fizzBuzzRule.Setup(y => y.GetContent()).Returns(string.Empty);
            FizzBuzzService service = new FizzBuzzService(new List<IFizzBuzzRule>() { this.fizzBuzzRule.Object });
            var actual = service.GetResult(0);
            Assert.AreEqual(actual, Enumerable.Empty<string>());
        }

        [TestCase(2, new string[] { "1", "2" })]
        public void FizzBuzzTestForTwo(int value, string[] expected)
        {
            this.fizzBuzzRule.Setup(x => x.IsValid(value)).Returns(false);
            this.fizzBuzzRule.Setup(y => y.GetContent()).Returns(string.Empty);
            FizzBuzzService service = new FizzBuzzService(new List<IFizzBuzzRule>() { this.fizzBuzzRule.Object });
            var result = service.GetResult(value);
            Assert.AreEqual(result, expected);
        }

        [TestCase(3, Constants.Fizz)]
        [TestCase(6, Constants.Fizz)]
        public void GetResultFizzTest(int value, string expected)
        {
            this.fizzBuzzRule.Setup(x => x.IsValid(It.IsAny<int>())).Returns(true);
            this.fizzBuzzRule.Setup(y => y.GetContent()).Returns("fizz");
            FizzBuzzService service = new FizzBuzzService(new List<IFizzBuzzRule>() { this.fizzBuzzRule.Object });
            var result = service.GetResult(value);
            var actual = result.ElementAt(2);

            Assert.AreEqual(actual, expected);
        }

        [TestCase(5, Constants.Buzz)]
        public void GetResultBuzzTest(int value, string expected)
        {
            this.fizzBuzzRule = new Mock<IFizzBuzzRule>();
            this.fizzBuzzRule.Setup(x => x.IsValid(It.IsAny<int>())).Returns(true);
            this.fizzBuzzRule.Setup(y => y.GetContent()).Returns("buzz");
            var service = new FizzBuzzService(new List<IFizzBuzzRule>() { this.fizzBuzzRule.Object });

            var result = service.GetResult(value);

            var actual = result.ElementAt(4);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(15, Constants.FizzBuzz)]
        [TestCase(30, Constants.FizzBuzz)]
        public void GetResultFizzBuzzTest(int value, string expected)
        {
            this.fizzBuzzRule = new Mock<IFizzBuzzRule>();
            this.fizzBuzzRule.Setup(x => x.IsValid(It.IsAny<int>())).Returns(true);
            this.fizzBuzzRule.Setup(y => y.GetContent()).Returns(expected);
            var service = new FizzBuzzService(new List<IFizzBuzzRule>() { this.fizzBuzzRule.Object });
            var result = service.GetResult(value);
            var actual = result.ElementAt(14);
            Assert.AreEqual(actual, expected);
        }

         
        [TestCase(3, (new string[] { "1", "2", "fizz" }))]
        [TestCase(5, (new string[] { "1", "2", "fizz","4","buzz"}))]       
        public void FizzBuzzTests(int value, string[] expected)
        {
            this.fizzRule.Setup(x => x.IsValid(3)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(6)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(9)).Returns(true);
            this.fizzRule.Setup(x => x.GetContent()).Returns("fizz");

            this.buzzRule.Setup(y => y.IsValid(5)).Returns(true);
            this.buzzRule.Setup(y => y.IsValid(10)).Returns(true);
            this.buzzRule.Setup(x => x.GetContent()).Returns("buzz");
            var service = new FizzBuzzService(new List<IFizzBuzzRule>() { this.fizzRule.Object, this.buzzRule.Object });
            var result = service.GetResult(value);
            Assert.AreEqual(result, expected);
        }
    }
}
