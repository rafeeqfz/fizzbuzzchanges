﻿namespace FizzBuzzWeb.Controllers
{
    using System.Web.Mvc;
    using FizzBuzzWeb.Models;
    using FizzBuzz.Services.Interfaces;
    using PagedList;

    public class FizzBuzzController : Controller
    {
        private const int PageSize = 20;
        private readonly IFizzBuzzService fizzBuzz;

        public FizzBuzzController(IFizzBuzzService fizzBuzz)
        {
            this.fizzBuzz = fizzBuzz;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return this.View(new FizzBuzzModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(FizzBuzzModel model)
        {
            return this.GetView(1, model);
        }

        public ActionResult Navigate(int input, int page)
        {
            var model = new FizzBuzzModel { Input = input };
            return GetView(page, model);
        }

        private ActionResult GetView(int page, FizzBuzzModel model)
        {
            if (this.ModelState.IsValid)
            {
                model.FizzBuzzNumbers = this.fizzBuzz.GetResult(model.Input).ToPagedList(page, PageSize);
            }
            return this.View("Index", model);
        }
    }
}