[assembly: WebActivator.PreApplicationStartMethod(typeof(FizzBuzzWeb.App_Start.StructuremapMvc), "Start")]

namespace FizzBuzzWeb.App_Start
{
    using System.Web.Http;
    using StructureMap;
    using System.Web.Mvc;
    using FizzBuzzWeb.DependencyResolution;

    public static class StructuremapMvc 
    {
        public static void Start() 
        {
			IContainer container = IoC.Initialize();
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);
        }
    }
}