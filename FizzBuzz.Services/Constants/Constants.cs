﻿namespace FizzBuzz.Services.Constants
{
  using System;

  public class Constants
    {
      public const DayOfWeek Week = DayOfWeek.Wednesday;
      public const int FizzDivisor = 3;
      public const int BuzzDivisor = 5;
      public const string Wizz = "wizz";
      public const string Fizz = "fizz";
      public const string Wuzz = "wuzz";
      public const string Buzz = "buzz";
      public const string FizzBuzz = "fizz buzz";
    }
}
