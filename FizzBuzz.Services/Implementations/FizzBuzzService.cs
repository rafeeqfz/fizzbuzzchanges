﻿namespace FizzBuzz.Services.Implementations
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzz.Services.Interfaces;

    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IEnumerable<IFizzBuzzRule> fizzBuzzRules;

        public FizzBuzzService(IEnumerable<IFizzBuzzRule> fizzBuzzRules)
        {
            this.fizzBuzzRules = fizzBuzzRules;
        }

        public IEnumerable<string> GetResult(int number)
        {
            // Enumerable.Range
            var fizzBuzzList = new List<string>();

            for (var fizzBuzzNumber = 1; fizzBuzzNumber <= number; fizzBuzzNumber++)
            {
                var rules = this.fizzBuzzRules.Where(a => a.IsValid(fizzBuzzNumber));
                var output = rules.Any() ? string.Join(" ", rules.Select(a => a.GetContent())) : fizzBuzzNumber.ToString();
                fizzBuzzList.Add(output);
            }
            return fizzBuzzList;
        }
    }
}