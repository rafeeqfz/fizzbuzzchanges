﻿namespace FizzBuzz.Services.Interfaces
{
    using System.Collections.Generic;

    public interface IFizzBuzzService
    {
        IEnumerable<string> GetResult(int number);
    }
}
