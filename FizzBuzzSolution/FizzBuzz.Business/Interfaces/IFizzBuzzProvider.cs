﻿namespace FizzBuzz.Business.Interfaces
{
    using System.Collections.Generic;

    public interface IFizzBuzzProvider
    {
        List<string> FizzBuzz(int number);
    }
}