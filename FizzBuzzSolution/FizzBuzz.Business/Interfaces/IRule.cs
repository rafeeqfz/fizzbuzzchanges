﻿namespace FizzBuzz.Business.Interfaces
{
    public interface IRule
    {
        bool IsDivisible(int number);

        string GetValue();
    }
}
