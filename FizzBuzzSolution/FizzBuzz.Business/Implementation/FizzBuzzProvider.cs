﻿namespace FizzBuzz.Business.Implementation
{
    using FizzBuzz.Business.Interfaces;
    using System.Collections.Generic;
    using System.Text;

    public class FizzBuzzProvider : IFizzBuzzProvider
    {

        private IEnumerable<IRule> rules;

        public FizzBuzzProvider(IEnumerable<IRule> rules)
        {
            this.rules = rules;
        }

        public List<string> FizzBuzz(int number)
        {
            var sequence = new List<string>();

            for (int count = 1; count <= number; count++)
            {

                var fizzbuzz = new StringBuilder();

                foreach (var items in this.rules)
                {

                    if (items.IsDivisible(count))
                    {
                        fizzbuzz.Append(items.GetValue()).Append(" ");
                    }
                }

                sequence.Add(fizzbuzz.Length > 0 ? fizzbuzz.ToString() : count.ToString());
            }

            return sequence;
        }
    }
}