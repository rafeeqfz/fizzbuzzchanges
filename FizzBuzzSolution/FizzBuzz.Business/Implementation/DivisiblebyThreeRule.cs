﻿namespace FizzBuzz.Business.Implementation
{
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Helper;

    public class DivisiblebyThreeRule : IRule
    {
        private const string Fizz = "Fizz";
        private const string Wizz = "Wizz";

        private readonly IWeekDayProvider weekDayProvider;

        public DivisiblebyThreeRule(IWeekDayProvider weekDayProvider)
        {
            this.weekDayProvider = weekDayProvider;
        }

        public string GetValue()
        {
            return this.weekDayProvider.IsWednesday() ? Wizz : Fizz;
        }

        public bool IsDivisible(int number)
        {
            return number % 3 == 0;
        }
    }
}