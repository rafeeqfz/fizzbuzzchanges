﻿namespace FizzBuzz.Business.Implementation
{
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Helper;

    public class DivisiblebyFiveRule : IRule
    {
        private const string Buzz = "Buzz";
        private const string Wuzz = "Wuzz";

        private readonly IWeekDayProvider weekDayProvider;

        public DivisiblebyFiveRule(IWeekDayProvider weekDayProvider)
        {
            this.weekDayProvider = weekDayProvider;
        }

        public string GetValue()
        {
            return this.weekDayProvider.IsWednesday() ? Wuzz : Buzz;
        }

        public bool IsDivisible(int number)
        {
            return number % 5 == 0;
        }
    }
}
