﻿namespace FizzBuzz.Web.Test
{
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Web.Controllers;
    using FizzBuzz.Web.Models;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    [TestFixture]
    public class FizzBuzzControllerTest
    {
        private Mock<IFizzBuzzProvider> mockFizzBuzzProvider;
        private FizzBuzzController fizzBuzzController;
        private int pageSize = 20;

        [SetUp]
        public void SetUp()
        {
            var resultList = new List<string> { "List" };
            this.mockFizzBuzzProvider = new Mock<IFizzBuzzProvider>();
            this.mockFizzBuzzProvider.Setup(x => x.FizzBuzz(It.IsAny<int>())).Returns(resultList);
            this.fizzBuzzController = new FizzBuzzController(this.mockFizzBuzzProvider.Object, this.pageSize);
        }

        [Test]
        public void Home_MethodTest()
        {
            var actualResult = this.fizzBuzzController.Home();
            Assert.IsNotNull(actualResult);
        }

        [Test]
        public void FizzBuzzPost_MethodTest()
        {
            var model = new FizzBuzzNumberModel();
            var actualResult = this.fizzBuzzController.FizzBuzzResult(1, model);
            var expectedResult = actualResult as ViewResult;
            Assert.NotNull(actualResult);
            Assert.AreSame(expectedResult.Model, model);
            Assert.AreEqual(expectedResult.ViewName, "Home");
        }

        [Test]
        public void ModelStateNotValidTest()
        {
            var model = new FizzBuzzNumberModel();
            this.fizzBuzzController.ModelState.AddModelError("Number", "Please enter the number between 1 to 1000");
            var actualResult = this.fizzBuzzController.FizzBuzzResult(1, model);
            var expectedResult = actualResult as ViewResult;
            Assert.IsNull(expectedResult.Model);
            Assert.AreEqual(expectedResult.ViewName, "Home");
        }
    }
}