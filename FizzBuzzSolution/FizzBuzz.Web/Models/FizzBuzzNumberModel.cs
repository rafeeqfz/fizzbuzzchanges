﻿namespace FizzBuzz.Web.Models
{
    using PagedList;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class FizzBuzzNumberModel
    {
        [Required]
        [Range(1, 1000, ErrorMessage = "Please enter the number between 1 to 1000")]
        public int Number { get; set; }

        public List<string> Result { get; set; }

        public IPagedList<string> Pages { get; set; }
    }
}