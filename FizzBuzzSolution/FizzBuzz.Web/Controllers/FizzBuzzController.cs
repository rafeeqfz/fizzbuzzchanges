﻿namespace FizzBuzz.Web.Controllers
{
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using PagedList;

    public class FizzBuzzController : Controller
    {
        private IFizzBuzzProvider fizzBuzzProvider;
        private int PageSize;

        public FizzBuzzController(IFizzBuzzProvider fizzBuzzProvider, int PageSize)
        {
            this.fizzBuzzProvider = fizzBuzzProvider;
            this.PageSize = PageSize;
        }

        [HttpGet]
        public ActionResult Home()
        {
            return this.View();
        }

        [HttpGet]
        public ActionResult FizzBuzzResult(int? page, FizzBuzzNumberModel fizzBuzzNumberModel)
        {
            if (this.ModelState.IsValid)
            {
                int pageIndex = 1;
                pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
                fizzBuzzNumberModel.Result = this.fizzBuzzProvider.FizzBuzz(fizzBuzzNumberModel.Number) as List<string>;
                fizzBuzzNumberModel.Pages = fizzBuzzNumberModel.Result.ToPagedList(pageIndex, this.PageSize);
                return this.View("Home", fizzBuzzNumberModel);
            }

            return this.View("Home");
        }
    }
}