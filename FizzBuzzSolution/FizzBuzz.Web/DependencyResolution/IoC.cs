namespace FizzBuzz.Web
{
    using System;
    using FizzBuzz.Business.Implementation;
    using FizzBuzz.Business.Interfaces;
    using FizzBuzz.Web.Controllers;
    using StructureMap;
    using System.Configuration;
    using FizzBuzz.Helper;

    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });
                            x.For<IFizzBuzzProvider>().Use<FizzBuzzProvider>();
                            x.For<IRule>().Add<DivisiblebyThreeRule>();
                            x.For<IRule>().Add<DivisiblebyFiveRule>();
                            x.For<IWeekDayProvider>().Use<WeekDayProvider>().Ctor<DayOfWeek>().Is((DayOfWeek)Enum.Parse(typeof(DayOfWeek), ConfigurationManager.AppSettings["WeekDay"]));
                            x.For<FizzBuzzController>().Use<FizzBuzzController>().Ctor<int>("PageSize").Is(Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]));
                        });
            return ObjectFactory.Container;
        }
    }
}