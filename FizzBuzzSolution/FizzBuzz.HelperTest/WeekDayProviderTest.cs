﻿namespace FizzBuzz.HelperTest
{
    using FizzBuzz.Helper;
    using NUnit.Framework;
    using System;

    [TestFixture]
    public class WeekDayProviderTest
    {
        public WeekDayProvider GetService(int days)
        {
            var weekday = DateTime.Today.AddDays(days).DayOfWeek;
            return new WeekDayProvider(weekday);
        }

        [TestCase(0, true)]
        [TestCase(1, false)]
        [TestCase(2, false)]
        [TestCase(3, false)]
        [TestCase(4, false)]
        [TestCase(5, false)]
        [TestCase(6, false)]
        public void DayofWeekProviderTestCheck(int days, bool result)
        {
            var actualResult = this.GetService(days).IsWednesday();

            Assert.AreEqual(result, actualResult);
        }
    }
}