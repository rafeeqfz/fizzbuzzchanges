﻿namespace FizzBuzz.Business.Test
{
    using FizzBuzz.Helper;
    using Implementation;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivisiblebyFiveRuleTest
    {
        private Mock<IWeekDayProvider> mockWeekDayProvider;
        private DivisiblebyFiveRule divisiblebyFiveRule;

        [SetUp]
        public void SetUp()
        {
            this.mockWeekDayProvider = new Mock<IWeekDayProvider>();
            this.divisiblebyFiveRule = new DivisiblebyFiveRule(this.mockWeekDayProvider.Object);
        }

        [TestCase(5, true)]
        [TestCase(6, false)]
        public void DivisiblebyFiveTestCheck(int number, bool expectedResults)
        {
            var actualResult = this.divisiblebyFiveRule.IsDivisible(number);
            Assert.AreEqual(expectedResults, actualResult);
        }

        [TestCase(true, "Wuzz")]
        [TestCase(false, "Buzz")]
        public void GetValue_BuzzifnotWednesday_WuzzifWednesday(bool value, string expectedResult)
        {
            this.mockWeekDayProvider.Setup(x => x.IsWednesday()).Returns(value);
            var divisiblebyFive = new DivisiblebyFiveRule(this.mockWeekDayProvider.Object);
            var actualResult = divisiblebyFive.GetValue();
            Assert.AreEqual(actualResult, expectedResult);
        }
    }
}