﻿namespace FizzBuzz.BusinessTest
{
    using FizzBuzz.Business.Implementation;
    using FizzBuzz.Business.Interfaces;
    using Moq;
    using NUnit.Framework;
    using System.Linq;
    using System.Collections.Generic;

    [TestFixture]
    public class FizzBuzzProviderTest
    {
        private FizzBuzzProvider fizzBuzzProvider;
        private Mock<IRule> mockDivisiblebyThreeRule;
        private Mock<IRule> mockDivisiblebyFiveRule;

        [SetUp]
        public void SetUp()
        {
            this.mockDivisiblebyThreeRule = new Mock<IRule>();
            this.mockDivisiblebyThreeRule.Setup(x => x.IsDivisible(It.Is<int>(i => i % 3 == 0))).Returns(true);

            this.mockDivisiblebyFiveRule = new Mock<IRule>();
            this.mockDivisiblebyFiveRule.Setup(x => x.IsDivisible(It.Is<int>(i => i % 5 == 0))).Returns(true);

            this.fizzBuzzProvider = new FizzBuzzProvider(new[]
            {
                this.mockDivisiblebyThreeRule.Object,
                this.mockDivisiblebyFiveRule.Object
            });
        }

        [TestCase(5)]
        public void FizzBuzzTest(int number)
        {
            var expectedResult = new List<string> { "1", "2", "Fizz ", "4", "Buzz " };
            this.mockDivisiblebyThreeRule.Setup(x => x.GetValue()).Returns("Fizz");
            this.mockDivisiblebyFiveRule.Setup(x => x.GetValue()).Returns("Buzz");
            var actualResult = this.fizzBuzzProvider.FizzBuzz(number);
            Assert.IsNotNull(actualResult);
            Assert.AreEqual(actualResult, expectedResult);
            Assert.AreEqual(actualResult.ToList().Count, number);
        }
    }
}
