﻿namespace FizzBuzz.Business.Test
{
    using FizzBuzz.Helper;
    using Implementation;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivisiblebyThreeTest
    {
        private Mock<IWeekDayProvider> mockWeekDayProvider;
        private DivisiblebyThreeRule divisiblebyThreeRule;

        [SetUp]
        public void SetUp()
        {
            this.mockWeekDayProvider = new Mock<IWeekDayProvider>();
            this.divisiblebyThreeRule = new DivisiblebyThreeRule(this.mockWeekDayProvider.Object);
        }

        [TestCase(6, true)]
        [TestCase(7, false)]
        public void DivisiblebyThreeTestCheck(int number, bool expectedResults)
        {
            var actualResult = this.divisiblebyThreeRule.IsDivisible(number);
            Assert.AreEqual(expectedResults, actualResult);
        }

        [TestCase(true, "Wizz")]
        [TestCase(false, "Fizz")]
        public void GetValue_FizzifnotWednesday_WizzifWednesday(bool value, string expectedResult)
        {
            this.mockWeekDayProvider.Setup(x => x.IsWednesday()).Returns(value);
            var divisiblebyThree = new DivisiblebyThreeRule(this.mockWeekDayProvider.Object);
            var actualResult = divisiblebyThree.GetValue();
            Assert.AreEqual(actualResult, expectedResult);
        }
    }
}