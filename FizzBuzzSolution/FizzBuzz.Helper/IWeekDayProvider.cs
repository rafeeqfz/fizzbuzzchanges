﻿namespace FizzBuzz.Helper
{
    public interface IWeekDayProvider
    {
        bool IsWednesday();
    }
}
