﻿namespace FizzBuzz.Helper
{
    using System;

    public class WeekDayProvider : IWeekDayProvider
    {

        private readonly DayOfWeek WeekDay;
        private readonly DayOfWeek currentDay;

        public WeekDayProvider(DayOfWeek WeekDay)
        {
            this.WeekDay = WeekDay;
            this.currentDay = DateTime.Now.DayOfWeek;
        }

        public bool IsWednesday()
        {
            return this.currentDay == this.WeekDay;
        }
    }
}